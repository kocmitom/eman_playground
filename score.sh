if [ -z "$INFER" ]; then
	echo "INFER"
	exit 0
fi

if [ -z "$REF" ]; then
	echo "REF"
	exit 0
fi

source s.tensor2tensor.8de32c2e.20180824-1610/tensor2tensor/env-cpu/bin/activate

cat $INFER/translated/00000 | sacrebleu -tok intl -lc --score-only $REF | tee /dev/tty > $INFER/score
cat $INFER/translated/00000 | sacrebleu -tok intl -lc $REF > $INFER/full_score

cat $INFER/score > $INFER/eman.status

