#!/usr/bin/env perl
use strict;
use warnings;

use utf8;
use autodie;
use Readonly;
binmode STDOUT, ':utf8';

my $preserve_case    = 0;
Readonly my $NGRAM_BLEU => 4;

print STDERR "Loading reference translation, counting ngrams (1 .. $NGRAM_BLEU)\n";
my $refer = shift;
open my $R,  '<:utf8', $refer;
my @refsent_ngram_count;
my @refsent_tokens;
my $sent = 0;
print "ref";
while(<$R>){
    my @tokens = split /\s+/, normalize_text($_);
    $refsent_tokens[$sent] = @tokens;
    foreach my $n ( 1 .. $NGRAM_BLEU ) {
        foreach my $ngram ( get_ngrams( $n, @tokens ) ) {
            $refsent_ngram_count[$sent]{$ngram}++;
        }
    }
    $sent++;
}
close $R;

# For every test translation file, print its name and count matching n-grams
print STDERR "Loading test translations:";
my @matching_system_sent_n_count;
my @tstsent_system_tokens;
my $system = 0;
while (my $test_fn = shift){
    open my $T, '<:utf8', $test_fn;
    $test_fn =~ s{^.*/(.+)(.txt)?}{$1};
    print "\t$test_fn";
    print STDERR "\t$test_fn";
    my $s=0;
    while(<$T>){
        my @tokens = split /\s+/, normalize_text($_);
        $tstsent_system_tokens[$s][$system] = @tokens;
        my %ref_count_of = %{$refsent_ngram_count[$s]};
        foreach my $n ( 1 .. $NGRAM_BLEU ) {
            foreach my $ngram ( get_ngrams( $n, @tokens ) ) {
                next if !$ref_count_of{$ngram};
                $ref_count_of{$ngram}--;
                $matching_system_sent_n_count[$system][$s][$n]++;
           }
        }
        $s++;
    }
    close $T;
    die "File $test_fn has $s sentences while $refer $sent." if $sent != $s;
    $system++;
}
print "\n";
print STDERR "\nPrinting to STDOUT...\n";

# For every sentence print one line: #ref_tokens \t matching_ngram_for_system1 \t ... \t matching_ngram_for_systemN
# where matching_ngram_for_system = #all_tokens_in_the_sentence #matching_unigrams #matching_bigrams ... #matching_NGRAM_BLEU-grams
foreach my $s (0 .. $sent-1){
  print $refsent_tokens[$s];
  foreach my $sys (0 .. $system-1){
    print "\t", join(' ', $tstsent_system_tokens[$s][$sys], map{$matching_system_sent_n_count[$sys][$s][$_]||0} (1 .. $NGRAM_BLEU));
  }
  print "\n";
}


#Verbatim copy of mteval-v11b.pl NormalizeText
sub normalize_text {
    my ($norm) = @_;

    # language-independent part:
    $norm =~ s/<skipped>//g;    # strip "skipped" tags
    $norm =~ s/-\n//g;          # strip end-of-line hyphenation and join lines
    $norm =~ s/\n/ /g;          # join lines
    $norm =~ s/&quot;/"/g;      # convert SGML tag for quote to "
    $norm =~ s/&amp;/&/g;       # convert SGML tag for ampersand to &
    $norm =~ s/&lt;/</g;        # convert SGML tag for less-than to >
    $norm =~ s/&gt;/>/g;        # convert SGML tag for greater-than to <

    # language-dependent part (assuming Western languages):
    $norm = " $norm ";
    $norm =~ tr/[A-Z]/[a-z]/ unless $preserve_case;
    $norm =~ s/([\{-\~\[-\` -\&\(-\+\:-\@\/])/ $1 /g;    # tokenize punctuation
    $norm =~ s/([^0-9])([\.,])/$1 $2 /g;                 # tokenize period and comma unless preceded by a digit
    $norm =~ s/([\.,])([^0-9])/ $1 $2/g;                 # tokenize period and comma unless followed by a digit
    $norm =~ s/([0-9])(-)/$1 $2 /g;                      # tokenize dash when preceded by a digit
    $norm =~ s/\s+/ /g;                                  # one space only between words
    $norm =~ s/^\s+//;                                   # no leading space
    $norm =~ s/\s+$//;                                   # no trailing space

    return $norm;
}

sub get_ngrams {
    my $n      = shift;
    my @ngrams = ();
    foreach my $start ( 0 .. @_ - $n ) {
        my $ngram = join ' ', @_[ $start .. $start + $n - 1 ];
        push @ngrams, $ngram;
    }
    return @ngrams;
}

# Copyright 2009 Martin Popel
# This file is distributed under the GNU General Public License v2. See $TMT_ROOT/README.