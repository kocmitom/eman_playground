#!/usr/bin/env perl
use strict;
use warnings;

use utf8;
use autodie;
use Readonly;
binmode STDOUT, ':utf8';

Readonly my $NGRAM_BLEU => 4;

my $total_ref_tokens = 0;
my @system_ngram_all;
my @system_ngram_matching;

while(<>){
    my ($ref_tokens, @tests) = split /\t/, $_;
    $total_ref_tokens += $ref_tokens;
    foreach my $system (0 .. $#tests){
        my ($tst_tokens, @matching) = split / /, $tests[$system];
        unshift @matching, 'fake';

        foreach my $n ( 1 .. $#matching ) {
            $system_ngram_all[$system][$n] += $tst_tokens - $n + 1;
            $system_ngram_matching[$system][$n] += $matching[$n];
        }
    }
}
die "No reference tokens to evaluate BLEU" if !$total_ref_tokens;

print join "\t", map {compute_bleu($_)} (0 .. $#system_ngram_all);
print "\n";

sub compute_bleu {
    my $system = shift;
    my $log_score = 0;
    die "No test tokens to evaluate BLEU for system $system" if !$system_ngram_all[$system][1];

    foreach my $n ( 1 .. $NGRAM_BLEU ) {

        # If no reference sentence was longer than n ...
        # This should not happen, but we never want to divide by 0.
        last if !$system_ngram_all[$system][$n];

        # If no matching n-gram in the whole date ...
        # This also should not normally happen (for n=4, more sentences and reasonable MT system)
        return 0 if !$system_ngram_matching[$system][$n];

        my $modified_prec = $system_ngram_matching[$system][$n] / $system_ngram_all[$system][$n];
        $log_score += log($modified_prec);
    }
    $log_score = $log_score / $NGRAM_BLEU;

    # Brevity penalty
    if ( $system_ngram_all[$system][1] < $total_ref_tokens ) {
        my $ratio = $total_ref_tokens / $system_ngram_all[$system][1];
        $log_score -= $ratio - 1;
    }
    return exp($log_score);
}

# Copyright 2009 Martin Popel
# This file is distributed under the GNU General Public License v2. See $TMT_ROOT/README.