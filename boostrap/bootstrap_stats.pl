#!/usr/bin/env perl
use strict;
use warnings;

my $ALPHA_LEVEL = shift;
$ALPHA_LEVEL = 0.05 if not defined $ALPHA_LEVEL;

my @system_sample_score;
my $sample = 0;

my $first_line = <>;
chomp $first_line;
my @names = split /\t/, $first_line;

while(<>){
  chomp;
  my @scores = split /\t/, $_;
  foreach my $system (0..$#scores){
    $system_sample_score[$system][$sample] = $scores[$system];
  }
  $sample++;
}
$sample--;
my $index_lower = int(($ALPHA_LEVEL / 2) * $sample);
my $index_upper = $sample - $index_lower;

my @system_system_diff;

foreach my $system (0..$#system_sample_score){
  my @scores = sort {$a <=> $b} @{$system_sample_score[$system]};
  printf "%-40s score=%.5f (%.5f to %.5f)", $names[$system], $system_sample_score[$system][0], $scores[$index_lower], $scores[$index_upper];
  @scores = ();
  for my $sys2 (0..$system-1){
    foreach my $i (0 .. $sample-1){
      $scores[$i] = $system_sample_score[$system][$i] - $system_sample_score[$sys2][$i];
    }
    my $avg = $scores[0];
    @scores = sort {$a <=> $b} @scores;
    $system_system_diff[$system][$sys2] = [$avg, $scores[$index_lower], $scores[$index_upper]];
  }
  if ($system == 0) { print "\n";}
  else { printf "  diff=%.5f (%.5f to %.5f)\n", @{$system_system_diff[$system][0]}; }
}

@names = map {substr($_,0,2)} @names;
print "BLEU significance matrix:\n  ", join(' ', @names), "\n";
foreach my $s1 (0..$#system_sample_score){
  print $names[$s1];
  foreach my $s2 (0..$#system_sample_score){
    if ($s1 == $s2) {print " x "; next;}
    my $diff = $s1 > $s2 ? $system_system_diff[$s1][$s2] : $system_system_diff[$s2][$s1];
    my $res  = $diff->[1] * $diff->[2] < 0          ? '?'
             : ($diff->[1] == 0 && $diff->[2] == 0) ? '='
             : ($s1 - $s2) * $diff->[0] < 0         ? '<'
             :                                        '>';
    print " $res ";
  }
  print "\n";
}

# Copyright 2009-2010 Martin Popel
# This file is distributed under the GNU General Public License v2. See $TMT_ROOT/README.