if [ -z "$STEP" ]; then echo "STEP"; exit 0; fi
if [ -z "$VALIDATE_STEP" ]; then echo "VALIDATE_STEP"; exit 0; fi
if [ -z "$TRANSLATION_FILE" ]; then echo "TRANSLATION_FILE and possibly REFERENCE"; exit 0; fi



eman init stept2t --start --ignore traint2t --ignore validatet2t 2>&1 | tee /dev/tty | grep Inited | sed 's/Inited: //' > .tmp.delme.step

export CHECKPOINT_STEP=$(cat .tmp.delme.step)
rm .tmp.delme.step


eman init inferencet2t --start

[ -z $TAG ] || eman addtag "$TAG" $CHECKPOINT_STEP
