#!/usr/bin/python

import sys, os, re

if len(sys.argv)!=2:
    sys.exit(0)

stop = False

with open(os.path.join(sys.argv[1], "scores")) as scores_file:
    scores2 = {}
    
    cont = False
    first_step = 999999999

    for line in scores_file:
        spl = line.strip().split('\t')
        step = int(re.sub(r'[^0-9]', '', spl[0]))
        if step < first_step:
            first_step = step

        scores2[step] = float(spl[2])

    scores = {}
    
    # subtract first step if the model continued
    for key in scores2:
        scores[key-first_step] = scores2[key]

    for maxim in sorted(scores.iterkeys()):
        prev_max = 0
        curr_max = 0
        eval_steps = 0
        best_so_far = 0

        for key in sorted(scores.iterkeys()):
            if key>maxim:
                break
            eval_steps+=1
            if scores[best_so_far]<scores[key]:
                best_so_far = key
            if key<maxim*0.5 and scores[key]>prev_max:
                prev_max = scores[key]
            if key>=maxim*0.5:
                if scores[key]>curr_max:
                    curr_max=scores[key]
        if eval_steps <= 5:
            continue
        if cont:
            best = best_so_far + first_step
        else:
            best = best_so_far
        print("step {} best {} ({}) - diff {}".format(maxim, best, scores[best_so_far], curr_max-prev_max))
        if curr_max-prev_max<prev_max*0.005 and not stop:
            print("FINISH THE EXPERIMENT ")
            stop = True
        


        
#    print(scores)

