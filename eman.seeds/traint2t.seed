# eman step for training tensor2tensor

## EMAN COMMON
source ~/.bashrc


## EMAN IGNORE
# Any part of the code labelled like thisis not used by eman at all, you can
# use it when running this script directly
# A useful thing to do is to prevent 'eman' commands from doing anything
alias eman="echo"
function die() { echo "$@" >&2; exit 1; }

## EMAN INIT
# eman variables get defined here
eman \
  defvar CONTINUESTEP type=varstep default='' \
    help='transformertrain step to inherit config from' \
  defvar DATAGENSTEP inherit=CONTINUESTEP \
    type=reqstep help='t2tdatagen step that prepares the data' \
  defvar TENSOR2TENSOR inherit=DATAGENSTEP \
    type=reqstep help='which Tensor2tensor step to use' \
  defvar PROBLEM inherit=DATAGENSTEP help='Name of a problem to create datasets' \
  defvar T2T_TMP_DIR inherit=DATAGENSTEP help='Tmp folder for creation of datasets.' \
  defvar T2T_USR_BASE inherit=DATAGENSTEP help='Where are problems defined.' \
  defvar MAX_TRAIN_STEPS inherit=CONTINUESTEP default='1000000' help='number of training steps' \
  defvar MODEL inherit=CONTINUESTEP default='transformer' help='Which model to use' \
  defvar MODEL_HPARAMS inherit=CONTINUESTEP default='transformer_big_single_gpu' help='What hparams' \
  defvar WARM_UP inherit=CONTINUESTEP default='16000' Help='Warmup steps' \
  defvar BATCH_SIZE inherit=CONTINUESTEP default='4500' help='Batch size in tokens' \
  defvar LEARNING_RATE inherit=CONTINUESTEP default='0.2' help='Learning rate' \
  defvar MAX_LENGTH inherit=CONTINUESTEP default='100' help='Maximal length to be used during training in tokens' \
  defvar CHECKPOINT_FREQUENCY inherit=CONTINUESTEP default='25000' help='Checkpoint frequence, save each X steps' \
  defvar KEEP_CHECKPOINT_MAX inherit=CONTINUESTEP default='1000' help='Maximum checkpoints to keep' \
  defvar OPTIMIZER inherit=CONTINUESTEP default='Adafactor' help='Optimizer either: Adam,Adafactor' \
  defvar SPECIAL_HPARAMS default='' inherit=CONTINUESTEP help='special hparams, must start with colon' \
  defvar EMAN_REMRUN default='' help='remote submission' \
  defvar EMAN_CORES default='' help='number of CPUs used by NM' \
  defvar EMAN_GPUS default='1' help='number of GPUs used by NM' \
  defvar EMAN_GPUMEM default='11G' help='demand GPU with this much RAM' \
  defvar EMAN_QUEUE default='gpu-ms.q@dll*,gpu-troja.q@tdll*' help="which queue to submit to" \
  defvar EMAN_MEM default='20G' help='required RAM size' \
  defvar EMAN_LIMITS default='' inherit=TENSOR2TENSOR help='special qsub -l flags' \
|| exit 1


if [[ "$OPTIMIZER" == "Adafactor" ]] && [[ $SPECIAL_HPARAMS != *learning_rate_schedule* ]]; then
    SPECIAL_HPARAMS=$SPECIAL_HPARAMS",learning_rate_schedule=rsqrt_decay"
    export SPECIAL_HPARAMS=$SPECIAL_HPARAMS
    eman defvar SPECIAL_HPARAMS
fi

if [ ! -z "$CONTINUESTEP" ]; then
   echo $CONTINUESTEP >> eman.deps 
fi

# we need to use vars here directly; run this to get also the default values
# from above
eval `eman bash-loadvars`

[ -z "$EMAN_CORES" ] || [ -z "$EMAN_GPUS" ] \
|| die "UFAL environment won't give you both more CPU and GPU cores"


## EMAN PREPARE
# eman variables are usable here

# symlink tensor2tensor
# copy would be preferred but it's not a single executable...
T2TPATH=$(eman path $TENSOR2TENSOR)
ln -s $T2TPATH/tensor2tensor ./tensor2tensor

mkdir models

## EMAN RUN
# if this code is idempotent, eman continue works best
set -ex # exit after any failure and show all commands run

if [ ! -z "$CONTINUESTEP" ] && [ -z "$(ls -A models)" ]; then
    if [ $(cat ../$CONTINUESTEP/eman.status) != "DONE" ]; then
        # the sleep is to let my pipeline to prepare
        sleep 20s
        die "The CONTINUESTEP is not finnished"
    fi
    # get the configuration from the previous run
    CONTINUEPATH=$(eman path $CONTINUESTEP)
    cp -r `ls $CONTINUEPATH/models/ | grep -v model | sed "s|^|$CONTINUEPATH/models/|"` models/
#model_checkpoint_path: "model.ckpt-1325000"
    LATEST=$(cat $CONTINUEPATH/models/checkpoint | grep "^model_checkpoint_path" | sed 's/^.*: "//' | sed 's/"$//')
    cp -r $CONTINUEPATH/models/$LATEST* models/

    # add steps from parent
    MAX_TRAIN_STEPS=$(($MAX_TRAIN_STEPS+$(echo $LATEST | sed 's/^.*ckpt-//')))
    export MAX_TRAIN_STEPS=$MAX_TRAIN_STEPS
    eman defvar MAX_TRAIN_STEPS
fi


if [[ $SPECIAL_HPARAMS != ','* ]] && [[ ! -z $SPECIAL_HPARAMS ]]; then
    SPECIAL_HPARAMS=","$SPECIAL_HPARAMS
fi


# Warning! since I introduced subshells in eman.command, even the environment
# has to be set up here in REMRUN, not in RUN

# Source the right virtual environment
source ./tensor2tensor/bashrc
source ./tensor2tensor/env-gpu/bin/activate

# emit log immediately
export PYTHONUNBUFFERED=yes
STEPDIR=`pwd`
DATAGEN_DIR="$STEPDIR/../$DATAGENSTEP"

DATA_DIR="$DATAGEN_DIR/data"
if [ -d "$T2T_USR_BASE" ]; then
  USR_BASE=$T2T_USR_BASE
else
  USR_BASE="$STEPDIR/../$T2T_USR_BASE"
fi

set +e # immediate exit clashes with the batch scaling loop
# Run the trainer

OTHER_PARAMS=''

if [[ $EMAN_GPUS > 0 ]]; then
	OTHER_PARAMS=$OTHER_PARAMS" --worker_gpu="$EMAN_GPUS

	assigned=`echo $CUDA_VISIBLE_DEVICES | sed 's/,/\t/g' | awk '{print NF}'`	
	if [[ $assigned != $EMAN_GPUS ]]; then
		die "The number of assigned GPUS does not match"
	fi
fi

SPECIAL_HPARAMS="batch_size="$BATCH_SIZE",learning_rate="$LEARNING_RATE",learning_rate_warmup_steps="$WARM_UP",max_length="$MAX_LENGTH",optimizer="$OPTIMIZER$SPECIAL_HPARAMS

tensor2tensor/tensor2tensor/bin/t2t-trainer \
    --t2t_usr_dir=$USR_BASE \
    --data_dir=$DATA_DIR \
    --problem=$PROBLEM \
    --model=$MODEL \
    --hparams_set=$MODEL_HPARAMS \
    --hparams=$SPECIAL_HPARAMS \
    --output_dir="models" \
    --train_steps=$MAX_TRAIN_STEPS \
    --local_eval_frequency=$CHECKPOINT_FREQUENCY \
    --schedule=train \
    --keep_checkpoint_max=$KEEP_CHECKPOINT_MAX \
    $OTHER_PARAMS




## EMAN FINISH
# whatever follows will be run after syncing remrun back here
# so here e.g. an output corpus can be registered
