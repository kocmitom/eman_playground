set -o xtrace


#if [ -z $SPECIAL_HPARAMS ]; then
#	export SPECIAL_HPARAMS="layer_prepostprocess_dropout=0.2,label_smoothing=0.2"
#else
#	export SPECIAL_HPARAMS=$SPECIAL_HPARAMS",layer_prepostprocess_dropout=0.2,label_smoothing=0.2"
#fi


[ ! -z $MAX_TRAIN_STEPS ] || export MAX_TRAIN_STEPS=100000
[ ! -z $CHECKPOINT_FREQUENCY ] || export CHECKPOINT_FREQUENCY=2500
#[ ! -z $AVERAGE_CHECKPOINTS ] || export AVERAGE_CHECKPOINTS=8

eman init traint2t --start 2>&1 | tee /dev/tty | grep Inited | sed 's/Inited: //' > .tmp.delme.step

sleep 2s

export TRAINER_STEP=$(cat .tmp.delme.step)
rm .tmp.delme.step

[ -z $TAG ] || eman addtag "$TAG" $TRAINER_STEP 


unset EMAN_GPUS
unset EMAN_GPUMEM
unset SPECIAL_HPARAMS
unset MAX_TRAIN_STEPS
eman init validatet2t --start --ignore traint2t 2>&1 | tee /dev/tty | grep Inited | sed 's/Inited: //' > .tmp.delme.step

sleep 2s

export VALIDATE_STEP=$(cat .tmp.delme.step)
eman init stept2t --start 2>&1 | tee /dev/tty | grep Inited | sed 's/Inited: //' > .tmp.delme.step

sleep 2s

srclng=`eman vars $DATAGENSTEP | grep SOURCE_LANG | cut -f2 --delim="="`
trglng=`eman vars $DATAGENSTEP | grep TARGET_LANG | cut -f2 --delim="="`

export TRANSLATION_FILE="tmp_data_t2t/test."$srclng$trglng".src"
export REFERENCE="tmp_data_t2t/test."$srclng$trglng".trg"
export CHECKPOINT_STEP=$(cat .tmp.delme.step)

eman init inferencet2t --start
