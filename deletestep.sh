#!/bin/bash

function die() { echo "$@" >&2; exit 1; }

CURTIME=$(date +%s)
FILETIME=$(stat eman.index -c %Y)
TIMEDIFF=$(expr $CURTIME - $FILETIME)
# Check if file older then 5s
if [ $TIMEDIFF -gt 5 ]; then
	echo "Reindexing ..."
	eman reindex
fi

for e in "$@"; do
  [ -d "$e" ] || die "Not a directory: $e"
  [ -e "$e/eman.command" ] || die "Not a step directory: $e"

  # recursively kill children
  for child in `eman traceforward $@ | sed 's/[+ |-]*//'`; do
	  if [ "$e" != "$child" ]; then
	      ./deletestep.sh $child
      fi
  done

  [ -d "$e" ] || continue

  for job in `qstat | tail -n +3 | cut -f1 -d ' '`; do 
	  cwd=`qstat -j $job | grep "sge_o_workdir"`
	  if [[ $cwd == *"$e"* ]]; then
		  qalter -m 'n' $job
		  qdel $job
	  fi
  done

  sleep 1s

  echo "Deleting step $e"
  rm -rf $e
  #  lastjid=$(ls $e/log.o* | sed 's/.*log.o//' | sort -nr | head -n 1)
  #  qdel $lastjid 2>&1 | sed "s/^/$e: /" >&2
done
