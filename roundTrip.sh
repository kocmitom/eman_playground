set -o xtrace

if [ -z "$FORWARD" ]; then
    echo "FORWARD step"
    exit 0
fi

if [ -z "$BACKWARD" ]; then
    echo "BACKWARD step"
    exit 0
fi

if [ -z "$FILE" ]; then
    echo "FILE to translate"
    exit 0
fi




export TRANSLATION_FILE=$FILE
export CHECKPOINT_STEP=$FORWARD

eman init inferencet2t --start 2>&1 | tee /dev/tty | grep Inited | sed 's/Inited: //' > .tmp.delme.step


forward_step=$(cat .tmp.delme.step)

export TRANSLATION_FILE=$forward_step/FINAL_TRANSLATION
export CHECKPOINT_STEP=$BACKWARD

eman init inferencet2t 2>&1 | tee /dev/tty | grep Inited | sed 's/Inited: //' > .tmp.delme.step

backward_step=$(cat .tmp.delme.step)

echo $forward_step >> $backward_step/eman.deps

eman start $backward_step

eman addtag fw $forward_step
eman addtag bw $backward_step
