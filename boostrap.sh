if [ -z "$SYSA" ]; then echo "Provide inference step for SYSA"; exit 0; fi
if [ -z "$SYSB" ]; then echo "Provide inference step for SYSB"; exit 0; fi


ref_A=`cat $SYSA/eman.vars | grep REFERENCE | sed 's/[^"]*"//' | sed 's/"//'`
ref_B=`cat $SYSB/eman.vars | grep REFERENCE | sed 's/[^"]*"//' | sed 's/"//'`

if [ "$ref_A" = "$ref_b" ]; then echo "References are not equal"; exit 0; fi

cat $ref_A > boostrap/SYS_ref
cat $SYSA/FINAL_TRANSLATION > boostrap/SYS_A
cat $SYSB/FINAL_TRANSLATION > boostrap/SYS_B

cd boostrap && make prepare


