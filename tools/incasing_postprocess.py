#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import fileinput


# do it as an automata 
# ≡ make next character upper
# ≡≡ make next word upper
# ≡≡≡ print ≡

for line in fileinput.input():
    counter=0
    for ch in line.rstrip('\r\n'):
        if ch=="≡":
            counter+=1

            if counter==3:
                print('≡', end='')
                counter=0
        else:
            if counter==0:
                print(ch, end='')
            elif counter==1:
                print(ch.upper(), end='')
                counter=0
            else:
                print(ch.upper(), end='')
                if ch==' ':
                    counter=0

    print('') # new line
