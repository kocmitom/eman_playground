#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import fileinput


# remove all ≡ from dataset, replace them with space. It should be sufficient for vocabulary creation
# lowercase everything

for line in fileinput.input():
    print(line.replace("≡", " ").lower(), end='')

