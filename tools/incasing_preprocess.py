#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import fileinput

# replace each ≡ character with ≡≡≡
# add ≡≡ before each fully upper cased word 
# add ≡ before each character that is uppercased


for line in fileinput.input():
    line = line.replace('≡', '≡≡≡')
    words = []
    for word in line.rstrip('\r\n').split(' '):
        if word.isupper() and len(word)>1:
            words.append("≡≡"+word.lower())
        else:
            tmp_word = ''
            for ch in word:
                if ch.isupper():
                    tmp_word += "≡"+ch.lower()
                else:
                    tmp_word += ch
            words.append(tmp_word)
    print(' '.join(words))
